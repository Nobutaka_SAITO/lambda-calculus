data Type                                                       =  Atomictype String | Type `Arrow` Type
     deriving (Eq)
instance Show Type where
         show (Atomictype a)                                      =  a
         show (domain `Arrow` range)                             =  "(" ++ show domain ++ "->" ++ show range ++ ")"

data Typed_term                                                 =  Var String Type | Typed_term `App` Typed_term | Abs Typed_term Typed_term
     deriving (Eq)

type Type_env                                                   =  [()]
instance Show Typed_term where
         show (Var name type0)                                  =  name ++ ":" ++ show type0
         show (left `App` right)                                =  "(" ++ show left ++ " " ++ show right ++ ")"
         show (Abs var term)                                    =  "(\\" ++ show var ++ "." ++ show term ++ ")"

--type_inference                                                  :: Typed_term -> Maybe Typed_term
--type_inference 
